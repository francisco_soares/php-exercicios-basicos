<?php

/**
 * EXERCÍCIO:
 * Construir um algoritmo que leia 2 números e efetue a adição. Caso o valor somado seja maior que 20, este deverá ser presentando somando-se a ele mais 8; caso o valor somado seja menor ou igual a 20, este deverá ser apresentado subtraindo-se 5.
 */
$msgErro = "";
$msgSucesso = "";

$numUm = isset($_GET['numUm']) ? $_GET['numUm'] : 0;
$numDois = isset($_GET['numDois']) ? $_GET['numDois'] : 0;

if ($numUm == '' && $numDois == '') {
    $msgErro = "<p class='erro'>Opss... Você precisa informar os <strong>Dois números</strong></p>";
} else {
    $numUm = floatval($numUm);
    $numDois = floatval($numDois);
    $soma = $numUm + $numDois;

    if ($numUm > 0 || $numDois > 0) {
        if ($soma > 20) {
            $total = $soma + 8;
            $msgSucesso = "
            <h3>Resultado:</h3>
            <p><strong>Soma dos dois valores:</strong> {$soma}</p>
            <p><strong>Somado + 8:</strong> {$total}</p>
            ";
        } else {
            $total = $soma - 5;
            $msgSucesso = "
            <h3>Resultado:</h3>
            <p><strong>Soma dos dois valores:</strong> {$soma}</p>
            <p><strong>Subtraido - 5:</strong> {$total}</p>
            ";
        }
    }
}

?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Soma de dois números</title>
    <link rel="stylesheet" href="style/style.css">
</head>

<body>

    <section class="container">
        <div class="content-90-780">

            <h1>Soma de <strong>dois Números</strong></h1>
            <form action="" method="GET">

                <a href="?">Atualizar</a>

                <?= $msgErro; ?>

                <label class="box100"><strong>Número 1:</strong>
                    <input type="text" name="numUm" placeholder="Informe o 1º número." />
                </label>

                <label class="box100"><strong>Número 2:</strong>
                    <input type="text" name="numDois" placeholder="Informe o 2º número." />
                </label>

                <div class="box100">
                    <input type="submit" value="Enviar" />
                </div>
                <div class="clear"></div>


                <?= $msgSucesso; ?>
            </form>

            <div class="clear"></div>
        </div>
    </section>

</body>

</html>