<?php

/**
 * EXERCÍCIO:
 * 1º - Crie um formulário com o campo: Nome da imagem e Enviar imagem
 *    - Os campos devem ser validados
 *    - O campo deve aceitar as imagens com as seguintes extensões: JPG, JPEG, PNG e GIF
 */

/**
 * FUNÇÕES USADAS
 * [str_replace]: https://www.w3schools.com/php/func_string_str_replace.asp
 * 
 * *********************
 * 
 * [mb_strstr ou strstr]: https://www.php.net/manual/pt_BR/function.mb-strstr.php
 * - Encontra a primeira ocorrência de uma string dentro de outra.
 * Ex: 
 * $img = logotipo.jpg
 * mb_strstr($img, '.')
 * Será pego a [STRING] da variável a partir do ponto, então fica assim: (.jpg)
 * 
 * *********************
 * 
 * [time]: https://www.php.net/manual/pt_BR/function.time.php
 * - Retorna o timestamp Unix atual
 * Ex: var_dump(time())
 * 
 * *********************
 * 
 * [in_array]: https://www.php.net/manual/pt_BR/function.in-array.php
 * - Checa se um valor existe em um array
 * Ex:
 * $os = array("Mac", "NT", "Irix", "Linux"); 
 * $teste = in_array("Irix", $os) - a variável $teste receberá o valor de TRUE
 * 
 * *********************
 * 
 * [file_exists]: https://www.php.net/manual/pt_BR/function.file-exists.php
 * É uma função integrada para verificar onde um diretório ou um arquivo existe ou não. Ela aceita um parâmetro de um caminho que retorna true se ele já existe ou false se não existir.
 * 
 * *********************
 * 
 * [is_dir]: https://www.php.net/manual/pt_BR/function.is-dir.php
 * Esta função também é semelhante a file_exists, e a única diferença é que ela só retornará true se a string passada for um diretório e retornará false se for um arquivo.
 * 
 * *********************
 * 
 * [mkdir]: https://www.php.net/manual/pt_BR/function.mkdir.php
 * Cria um diretório
 * 
 * *********************
 * 
 * [move_uploaded_file]
 * 
 */

$msgErro = "";
$msgSucesso = "";

// Antes de pegar os dados enviado via Formulário, eu faço uma validação para verificar se realmente foi enviado os dados.
$nomeArquivo = (isset($_POST['nome_arquivo']) && !empty($_POST['nome_arquivo'])) ? $_POST['nome_arquivo'] : 0;

/* [FILE] só funciona com método [POST] definido no Formulário */
/* O [FILE] retorna as seguintes informações no [Array]
    [name] - Nome completo do arquivo com a extensão
    [type] - Tipo do arquivo. Caso seja enviado um arquivo PDF o tipo será [application/pdf], PNG [image/png], JPG [image/jpeg]
    [tmp_name] - é o local onde o arquivo fica salvo TEMPORÁRIAMENTE
    [error] - Retorna um código de erro caso tenha um problema de um erro
    [size] - Retorna o tamanho do arquivo   
*/
$img = (isset($_FILES['imagem']) && !empty($_FILES['imagem'])) ? $_FILES['imagem'] : 0;

echo $nomeArquivo;

// Defino um Array com as extensões que serão aceitas
$imgMimeType = [
    "image/jpeg",
    "image/png",
    "image/gif"
];

/* Faço uma condição verificando se as informações enviadas pelo formulário estão vazias, caso esteja mostre o erro abaixo. */
if ($nomeArquivo == "" || $img == "") {
    $msgErro = "<p class='erro'>Opss... Você precisa informar o <strong>NOME DA IMAGEM e IMAGEM</strong></p>";
} else {
    if ($nomeArquivo != 0 && $img != 0) {

        /* Verifico se o arquivo enviado é diferente de [JPG, PNG ou GIF], caso seja, apresenta o erro. */
        if (!in_array($img['type'], $imgMimeType)) {
            $msgErro = "<p class='erro'>Opss... Você deve enviar arquivos no formato <strong>JPG, PNG ou GIF!</strong></p>";
        } else {
            // Pego o nome passado na váriavel [$nomeArquivo] e concateno com [.extensao], já que na função [mb_strstr] estou pedindo para pegar
            // do arquivo [$img['name']] a partir do [.] para frente.
            $imgArquivo = $nomeArquivo . "_" . time() . mb_strstr($img['name'], '.');

            // Aqui faço uma tratativa para retirar caracteres especiais
            $comAcentos = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜüÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿRr"!@#$%&*()+={[}]/?;:,\\\'<>°ºª';
            $subSemAcento = 'aaaaaaaceeeeiiiidnoooooouuuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr_______________________________';
            $imgArquivo = strtr(utf8_decode($imgArquivo), utf8_decode($comAcentos), $subSemAcento);

            // Abaixo retiro os traços e substituo pelo underline
            $imgArquivo = str_replace('-', '_', $imgArquivo);
            // Abaixo retiro os espaços em branco e substituo po underline
            $imgArquivo = str_replace(' ', '_', $imgArquivo);
            // Abaixo deixo o nome em minusculo.
            $imgArquivo = strtolower($imgArquivo);

            /* Verifico se o tamanho do arquivo e menor que 100KB e verifico se é o arquivo correto, caso seja, entra na condição. */
            if ($img['size'] < 100000 && in_array($img['type'], $imgMimeType)) {

                // Verifico se o diretório já foi criado, caso não seja eu o crio.
                $pasta = __DIR__ . "/img";

                /* A função [file_exists] é uma função integrada para verificar onde um diretório ou um arquivo existe ou não. Ela aceita um parâmetro de um caminho que retorna true se ele já existe ou false se não existir. */

                /* [is_dir] - Esta função também é semelhante a file_exists, e a única diferença é que ela só retornará true se a string passada for um diretório e retornará false se for um arquivo. */
                if (!file_exists($pasta) || !is_dir($pasta)) {
                    /* [PERMISSÂO DA PASTA] 
                    - 0600: Escrita e leitura para o proprietário, nada para os outros;
                    - 0644: Escrita e leitura para o proprietário, leitura para todos os outros;
                    - 0755: Tudo para o proprietário, leitura e execução para os outros;
                    - 0750: Tudo para o proprietário, leitura e execução para o grupo do proprietário.
                    */
                    mkdir($pasta, 0755);
                }

                // Com o [move_uploaded_file] eu movo o arquivo para a pasta [CV]
                move_uploaded_file($img['tmp_name'], __DIR__ . "/img/{$imgArquivo}");

                // Imprimo a mensagem de sucesso.
                $msgSucesso = "<h3>SUCESSO</h3>
                <p>Imagem enviada com sucesso.</p>";
            } else {
                $msgErro = "<p class='erro'>Opss... o tamanho não pode passar de <strong>100KB</strong></p>";
            }
        }
    }
}

?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Envio de Arquivos JPG, PNG e GIF</title>
    <link rel="stylesheet" href="style/style.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>

<body>
    <section class="container">
        <div class="content-90-780">

            <h1>Envio de Arquivos <strong>JPG, PNG e GIF</strong></h1>
            <form action="" method="POST" enctype="multipart/form-data">
                <a href="?">Atualizar</a>

                <?= $msgErro; ?>

                <label class="box100"><strong>Nome imagem:</strong>
                    <input type="text" name="nome_arquivo" placeholder="Ex: foto no parque" />
                </label>

                <label class="box100 send-file" id="label_file" title="Clique aqui para enviar seu CV"><strong>Enviar imagem:</strong>
                    <input type="file" name="imagem" id="img_file">
                    <span class="material-icons">image</span>
                </label>

                <div class="box100">
                    <input type="submit" value="Enviar" />
                </div>
                <div class="clear"></div>

                <?= $msgSucesso; ?>
            </form>

            <div class="clear"></div>
        </div>
    </section>

    <!-- Script de alteração do campo FILE -->
    <script>
        // Pego o ID co campo [img_file]
        let img = document.getElementById('img_file')

        img.onchange = function() {
            // Pego o valor que vem da variavel [img]
            let imgFile = img.value
            // Retiro o [C:\\fakepath\\] do caminho que apresenta quando inserido o arquivo
            imgFile = imgFile.replace('C:\\fakepath\\', '')

            // Crio o elemento [SPAN] no DOM
            arquivoSelecionado = document.createElement('span')
            // Crio a classe [arquivo_selecionado] dentro do [SPAN]
            arquivoSelecionado.classList.add('arquivo_selecionado')
            // Formato a cor do texto para uma cor cinza
            arquivoSelecionado.style.color = "#555"
            // Inserido o texto dentro do [SPAN]
            arquivoSelecionado.innerHTML = "Arquivo <strong>" + imgFile + "</strong> selecionado."

            // Insiro o [SPAN] dentro da [label_file]
            document.getElementById('label_file').appendChild(arquivoSelecionado)

            // Verifico se a classe [arquivo_selecionado] existe dentro do DOM.
            if (document.getElementsByClassName('arquivo_selecionado')) {
                // Selecio a classe [arquivo_selecionado]
                removeClasse = document.querySelector('.arquivo_selecionado')
                // Removo a classe [arquivo_selecionado]
                removeClasse.parentNode.removeChild(removeClasse);
            }
            // Insiro novamente o [SPAN] dentro da [label_file]
            document.getElementById('label_file').appendChild(arquivoSelecionado)
        }
    </script>

</body>

</html>