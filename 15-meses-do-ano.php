<?php

/**
 * EXERCÍCIO:
 * Ler um número inteiro entre 1 e 12 e escrever o mês correspondente. Caso o número seja fora desse intervalo, informar que não existe mês com este número.
 */
$msgErro = "";
$msgSucesso = "";

$mes = isset($_GET['mes']) ? $_GET['mes'] : 0;

if ($mes == "") {
    $msgErro = "<p class='erro'>Opss... Você precisa informar os <strong>Dois números</strong></p>";
} else {
    $meses = [
        "1" => "janeiro", "2" => "fevereiro", "3" => "março", "4" => "abril", "5" => "maio", "6" => "junho",
        "7" => "julho", "8" => "agosto", "9" => "setembro", "10" => "outubro", "11" => "novembro", "12" => "dezembro"
    ];

    if ($mes > 0) {

        foreach ($meses as $key => $value) {

            switch ($mes) {
                case $key:
                    $msgSucesso = "
                    <h3>Resultado:</h3>
                    <p>Mês escolhido: <strong>" . ucfirst($value) . "</strong></p>
                    ";
                    

                case $value:
                    $msgSucesso = "
                    <h3>Resultado:</h3>
                    <p>Mês escolhido: <strong>" . ucfirst($value) . "</strong></p>
                    ";
                    break;

            }
            // Modo 2
            // if ($key == $mes) {
            //     $msgSucesso = "
            //     <h3>Resultado:</h3>
            //     <p>Mês escolhido: <strong>" . ucfirst($value) . "</strong></p>
            //     ";
            //     break;
            // } elseif ($value == $mes) {
            //     $msgSucesso = "
            //     <h3>Resultado:</h3>
            //     <p>Mês escolhido: <strong>" . ucfirst($value) . "</strong></p>
            //     ";
            //     break;
            // } else {
            //     $msgSucesso = "
            //     <h3>Resultado:</h3>
            //     <p>Mês escolhido <strong>não existe.</strong></p>
            //     ";
            // }
        }
    }
}


?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Meses do Ano</title>
    <link rel="stylesheet" href="style/style.css">
</head>

<body>

    <section class="container">
        <div class="content-90-780">

            <h1>Meses do <strong>Ano</strong></h1>
            <form action="" method="GET">

                <a href="?">Atualizar</a>

                <?= $msgErro; ?>

                <label class="box100"><strong>Mês:</strong>
                    <input type="text" name="mes" placeholder="Informe o número do mês. Ex: 2" />
                </label>

                <div class="box100">
                    <input type="submit" value="Enviar" />
                </div>
                <div class="clear"></div>


                <?= $msgSucesso; ?>
            </form>

            <div class="clear"></div>
        </div>
    </section>

</body>

</html>