<?php

// Fórmula: (<valor> °C × 9/5) + 32 = 50 °F

/** [DICA]
 * ISSET: Verifica se a variável ou outro elemento existe. 
 * EMPTY: verifica se contém algum valor na variável (se ela esta vazia ou não).
 */

$msgErro = "";
$msgSucesso = "";

$grau = isset($_GET['grau']) ? $_GET['grau'] : 0;

if ($grau == "") {
    $msgErro = "<p class='erro'>Opss... Você precisa informar o <strong>Grau.</strong></p>";
} else {

    if ($grau > 0) {
        $conversao = ($grau * 9 / 5) + 32;

        $msgSucesso = "
        <h3>Resultado da conversão:</h3>
        <p><strong>Grau em Celsius:</strong> {$grau}</p>
        <p><strong>Grau em Fahrenheit:</strong> {$conversao}</p>
        ";
    }
}

?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Celsius para Fahrenheit</title>
    <link rel="stylesheet" href="style/style.css">
</head>

<body>

    <section class="container">
        <div class="content-90-780">

            <h1>Celsius para <strong>Fahrenheit</strong></h1>
            <form action="" method="GET">

                <a href="?">Atualizar</a>

                <?= $msgErro; ?>

                <label class="box100"><strong>Grau em Celsius:</strong>
                    <input type="text" name="grau" placeholder="Informe o grau em Celsius." />
                </label>

                <div class="box100">
                    <input type="submit" value="Enviar" />
                </div>
                <div class="clear"></div>


                <?= $msgSucesso; ?>
            </form>

            <div class="clear"></div>
        </div>
    </section>

</body>

</html>