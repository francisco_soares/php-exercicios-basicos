<?php

$msgErro = "";
$msgSucesso = "";

$nota1 = isset($_GET['nota1']) ? floatval($_GET['nota1']) : -0.1;
$nota2 = isset($_GET['nota2']) ? floatval($_GET['nota2']) : -0.1;
$nota3 = isset($_GET['nota3']) ? floatval($_GET['nota3']) : -0.1;

if ($nota1 != "" && $nota2 != "" && $nota3 != "") {    
    
    if ($nota1 >= 0 && $nota2 >= 0 && $nota3 >= 0) {
        $media = ($nota1 + $nota2 + $nota3) / 3;

        if ($media < 5) {
            $msgSucesso = "
            <h3>Resultado:</h3>
            <p>O Aluno não passou esse semestre! <strong>A média dele foi {$media}.</strong></p>
            ";
        } elseif ($media == 5) {
            $msgSucesso = "
            <h3>Resultado:</h3>
            <p>O Aluno passou por pouco! <strong>A média dele foi {$media}.</strong></p>
            ";
        } else {
            $msgSucesso = "
            <h3>Resultado:</h3>
            <p>O Aluno passou! <strong>A média dele foi {$media}.</strong></p>
            ";
        }
    }
} else {
    $msgErro = "<p class='erro'>Opss... Você precisa informar as <strong>3 notas do aluno.</strong></p>";
}

?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Média do Aluno</title>
    <link rel="stylesheet" href="style/style.css">
</head>

<body>

    <section class="container">
        <div class="content-90-780">

            <h1>Média do <strong>Aluno</strong></h1>
            <form action="" method="GET">

                <a href="?">Atualizar</a>

                <?= $msgErro; ?>

                <label class="box100"><strong>Nota 1</strong>
                    <input type="text" name="nota1" placeholder="Informe a 1ª nota do Aluno." />
                </label>

                <label class="box100"><strong>Nota 2</strong>
                    <input type="text" name="nota2" placeholder="Informe a 2ª nota do Aluno." />
                </label>

                <label class="box100"><strong>Nota 3</strong>
                    <input type="text" name="nota3" placeholder="Informe a 3ª nota do Aluno." />
                </label>

                <div class="box100">
                    <input type="submit" value="Enviar" />
                </div>
                <div class="clear"></div>


                <?= $msgSucesso; ?>
            </form>

            <div class="clear"></div>
        </div>
    </section>

</body>

</html>