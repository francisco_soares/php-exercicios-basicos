<?php

// Fórmulas: Para homens: (72.7 * Altura) - 58 | Para mulheres: (62.1 * Altura) - 44.7

/** [DICA]
 * ISSET: Verifica se a variável ou outro elemento existe. 
 * EMPTY: verifica se contém algum valor na variável (se ela esta vazia ou não).
 */

$msgErro = "";
$msgSucesso = "";

$altura = isset($_GET['altura']) ? $_GET['altura'] : 0;
$sexo = isset($_GET['sexo']) ? $_GET['sexo'] : '';

var_dump([
    "Altura" => $altura,
    "Sexo" => $sexo
]);

if ($altura == "" && $sexo == "") {
    $msgErro = "<p class='erro'>Opss... Você precisa informar a sua <strong>ALTURA</strong> e <strong>SEXO</strong></p>";
} else {

    $altura = floatval($altura);

    // Retiro o PONTO(.) caso o usuário coloque
    $strAltura = str_replace('.', '', $altura);
    // Coloco o PONTO(.) depois de retirar no código acima.
    $substrAltura = substr_replace($strAltura, '.', 1, 0);

    if ($sexo == "h" && $altura > 1) {
        $peso = (72.7 * $substrAltura) - 58;
        $msgSucesso = "
            <h3>Resultado</h3>
            <p><strong>Sua altura:</strong> {$substrAltura}</p>
            <p><strong>Seu peso:</strong> {$peso}</p>
            ";
    }
    if ($sexo == "m" && $altura > 1) {
        $peso = (62.1 * $substrAltura) - 44.7;
        $msgSucesso = "
            <h3>Resultado</h3>
            <p><strong>Sua altura:</strong> {$substrAltura}</p>
            <p><strong>Seu peso:</strong> {$peso}</p>";
    }
}
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Calculo do Peso Ideal</title>
    <link rel="stylesheet" href="style/style.css">
</head>

<body>

    <section class="container">
        <div class="content-90-780">

            <h1>Calculo do <strong>Peso Ideal</strong></h1>
            <form action="" method="GET">

                <a href="?">Atualizar</a>

                <?= $msgErro; ?>

                <label class="box100"><strong>Altura:</strong>
                    <input type="text" name="altura" placeholder="Informe sua altura." />
                </label>

                <label class="box100"><strong>Você é?</strong><br>
                    Homem <input type="radio" name="sexo" value="h" />
                    Mulher <input type="radio" name="sexo" value="m" />
                </label>

                <div class="box100">
                    <input type="submit" value="Enviar" />
                </div>
                <div class="clear"></div>


                <?= $msgSucesso; ?>
            </form>

            <div class="clear"></div>
        </div>
    </section>

</body>

</html>