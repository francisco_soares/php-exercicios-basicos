<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: PUT, GET, POST");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");

$jSON = [];
$path = "img";
$name = "blob-to-png-" . time() . ".png";
$blob = json_decode(file_get_contents('php://input'));
$image = preg_replace('#^data:image/[^;]+;base64,#', '', $blob->blob);

$file = fopen($path . "/" . $name, "w");
fwrite($file, base64_decode($image));
fclose($file);

$file_true = $path . "/" . $name;
if (file_exists($file_true)) {
    $jSON['status'] = true;
} else {
    $jSON['status'] = false;
}

header('Content-Type: application/json; charset=utf-8');
echo json_encode($jSON);
