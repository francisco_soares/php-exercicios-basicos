<?php

$msgErro = "";
$msgSucesso = "";

$metro = isset($_GET['metro']) ? $_GET['metro'] : 0;

if ($metro == '') {
    $msgErro = "<p class='erro'>Opss... Você precisa informar o <strong>número.</strong></p>";
} else {
    $metro = floatval($metro);

    if ($metro > 0 && is_float($metro)) {
        $cm = $metro * 100;

        $msgSucesso = "
        <h3>Resultado da conversão</h3>
        <p><strong>{$metro} metro(s)</strong> é igual á <strong>{$cm} centimetro(s)</strong></p>
        ";
    }
}
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Convertendo Metros para Centimetros</title>
    <link rel="stylesheet" href="style/style.css">
</head>

<body>

    <section class="container">
        <div class="content-90-780">

            <h1>Convertendo <strong>Metros para Centimetros</strong></h1>
            <form action="" method="GET">

                <a href="?">Atualizar</a>

                <?= $msgErro; ?>

                <label class="box100"><strong>Metro</strong>
                    <input type="text" name="metro" placeholder="Informe o metro." />
                </label>

                <div class="box100">
                    <input type="submit" value="Converter" />
                </div>
                <div class="clear"></div>


                <?= $msgSucesso; ?>
            </form>

            <div class="clear"></div>
        </div>
    </section>

</body>

</html>