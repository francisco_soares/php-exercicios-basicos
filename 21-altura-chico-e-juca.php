<?php

/**
 * EXERCÍCIO:
 * Chico tem 1,50m e cresce 2 centímetros por ano, enquanto Juca tem 1,10m e cresce 3 centímetros por ano. 
 * Construir um algoritmos que calcule e imprima quantos anos serão necessários para que Juca seja maior que Chico
 */

/**
 * FUNÇÕES USADAS
 * [substr_replace]: Substitui o texto dentro de uma parte de uma string | Link: https://www.w3schools.com/php/func_string_substr_replace.asp
 * [strpos]: https://www.w3schools.com/php/func_string_strpos.asp
 * [str_replace]: https://www.w3schools.com/php/func_string_str_replace.asp
 */

$msgErro = "";
$msgSucesso = "";

$altura = isset($_GET['altura']) ? $_GET['altura'] : 0;
$alturaComparada = isset($_GET['alturaComparada']) ? $_GET['alturaComparada'] : 0;
$ano = 0;

if ($altura == "" || $alturaComparada == "") {
    $msgErro = "<p class='erro'>Opss... Você precisa informar sua <strong>Altura</strong> e <strong>altura que quer chegar</strong></p>";
} else {
    if (strpos($altura, ',')) {
        $altura = str_replace(',', '', $altura);
    }
    if (strpos($alturaComparada, ',')) {
        $alturaComparada = str_replace(',', '', $alturaComparada);
    }    

    $altura = floatval($altura);
    $alturaComparada = floatval($alturaComparada);

    $altura = str_replace('.', '', $altura);
    $altura = substr_replace($altura, '.', 1, 0);
    $alturaAtual = $altura;

    $alturaComparada = str_replace('.', '', $alturaComparada);
    $alturaComparada = substr_replace($alturaComparada, '.', 1, 0);
    $alturaComparadaAtual = $alturaComparada;

    if (strlen($alturaAtual) == 3) {
        $alturaAtual = substr_replace($alturaAtual, '0', 3, 0);
    }
    if (strlen($alturaComparadaAtual) == 3) {
        $alturaComparadaAtual = substr_replace($alturaComparadaAtual, '0', 3, 0);
    }

    if (strlen($alturaAtual) <= 4 && strlen($alturaComparada) <= 4) {
        if ($altura > 0 && $alturaComparada > 0) {
            while ($alturaComparada > $altura) {
                $altura += 0.03;
                $ano++;
            }

            $msgSucesso = "
            <h3>Resultado final:</h3>
            <p><strong>Sua altura agora:</strong> {$alturaAtual}</p>
            <p><strong>Altura que você quer chegar:</strong> {$alturaComparadaAtual}</p>
            <p><strong>Quantos anos para chegar a altura:</strong> {$ano}</p>
            ";
        }
    } else {
        $msgErro = "<p class='erro'>Opss... você precisa informar um número da seguinte forme <strong>165 ou 1.65</strong></p>";
    }
}
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Informe sua altura</title>
    <link rel="stylesheet" href="style/style.css">
</head>

<body>
    <section class="container">
        <div class="content-90-780">

            <h1>Calculo de <strong>Altura</strong></h1>
            <form action="" method="GET">
                <a href="?">Atualizar</a>

                <?= $msgErro; ?>

                <label class="box100"><strong>Sua altura:</strong>
                    <input type="text" name="altura" placeholder="Ex: 1.65 ou 165" />
                </label>
                <label class="box100"><strong>Altura que quer checar:</strong>
                    <input type="text" name="alturaComparada" placeholder="Ex: 1.8 ou 180" />
                </label>

                <div class="box100">
                    <input type="submit" value="Enviar" />
                </div>
                <div class="clear"></div>


                <?= $msgSucesso; ?>
            </form>

            <div class="clear"></div>
        </div>
    </section>

</body>

</html>