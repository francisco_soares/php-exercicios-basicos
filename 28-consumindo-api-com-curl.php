<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Consumindo API com cURL</title>

    <!-- Importando bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>

<body>

    <?php
    // Recebo a url da API e seto na variavel $url_api.
    $url_api = 'https://swapi.dev/api/people/?page=2';
    // Inicio o cURL e passo a url da API, isso seria a variavel $url_api
    $curl_api = curl_init($url_api);

    // Convertendo para Array
    curl_setopt($curl_api, CURLOPT_RETURNTRANSFER, true);

    // Verificando o SSL, como está definido como false, ele não verificar se tem SSL
    curl_setopt($curl_api, CURLOPT_SSL_VERIFYPEER, false);

    // Tipo de requisição
    curl_setopt($curl_api, CURLOPT_CUSTOMREQUEST, 'GET');

    // Executo o cURL com o método curl_exec(), passando por parametro o cURL iniciada[$curl_api]
    // Na variavel $result, o mesmo vai trazer um obj.
    $result = json_decode(curl_exec($curl_api));



    foreach ($result->results as $actor) {
        echo "<strong>Nome:</strong> {$actor->name} <br>";
        echo "<strong>Altura:</strong> {$actor->height} <br>";
        echo "<strong>Filmes:</strong> ";

        foreach ($actor->films as $films) {

            $curl_api_films = curl_init($films);

            // Convertendo para Array
            curl_setopt($curl_api_films, CURLOPT_RETURNTRANSFER, true);

            // Verificando o SSL, como está definido como false, ele não verificar se tem SSL
            curl_setopt($curl_api_films, CURLOPT_SSL_VERIFYPEER, false);

            // Tipo de requisição
            curl_setopt($curl_api_films, CURLOPT_CUSTOMREQUEST, 'GET');

            // Executo o cURL com o método curl_exec(), passando por parametro o cURL iniciada[$curl_api]
            // Na variavel $result, o mesmo vai trazer um obj.
            $result_films = json_decode(curl_exec($curl_api_films));

            // var_dump($result_films);

            echo "{$result_films->title}, ";
        }

        echo "<hr>";
    }
    ?>
    
    <!-- Importando bootstrap -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>

</html>