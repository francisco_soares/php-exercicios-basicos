<?php

/**
 * EXERCÍCIO:
 * 1º - Crie um formulário com os campos: Nome, E-mail, Telefone e Enviar CV
 *    - O todos os campos devem ser validado
 *    - O campo ENVIAR CV deve aceitar arquivos com as seguintes extensões: DOC e PDF
 *    - Salvar os arquivos em uma pasta chamada CV
 */

/**
 * FUNÇÕES USADAS
 * [str_replace]: https://www.w3schools.com/php/func_string_str_replace.asp
 * 
 * *********************
 * 
 * [mb_strstr ou strstr]: https://www.php.net/manual/pt_BR/function.mb-strstr.php
 * - Encontra a primeira ocorrência de uma string dentro de outra.
 * Ex: 
 * $img = logotipo.jpg
 * mb_strstr($img, '.')
 * Será pego a [STRING] da variável a partir do ponto, então fica assim: (.jpg)
 * 
 * *********************
 * 
 * [time]: https://www.php.net/manual/pt_BR/function.time.php
 * - Retorna o timestamp Unix atual
 * Ex: var_dump(time())
 * 
 * *********************
 * 
 * [in_array]: https://www.php.net/manual/pt_BR/function.in-array.php
 * - Checa se um valor existe em um array
 * Ex:
 * $os = array("Mac", "NT", "Irix", "Linux"); 
 * $teste = in_array("Irix", $os) - a variável $teste receberá o valor de TRUE
 * 
 * *********************
 * 
 * [file_exists]: https://www.php.net/manual/pt_BR/function.file-exists.php
 * É uma função integrada para verificar onde um diretório ou um arquivo existe ou não. Ela aceita um parâmetro de um caminho que retorna true se ele já existe ou false se não existir.
 * 
 * *********************
 * 
 * [is_dir]: https://www.php.net/manual/pt_BR/function.is-dir.php
 * Esta função também é semelhante a file_exists, e a única diferença é que ela só retornará true se a string passada for um diretório e retornará false se for um arquivo.
 * 
 * *********************
 * 
 * [mkdir]: https://www.php.net/manual/pt_BR/function.mkdir.php
 * Cria um diretório
 * 
 * *********************
 * 
 * [move_uploaded_file]
 * 
 */

$msgErro = "";
$msgSucesso = "";

// Antes de pegar os dados enviado via Formulário, eu faço uma validação para verificar se realmente foi enviado os dados.
$nome = isset($_POST['nome']) && isset($_POST['nome']) ? $_POST['nome'] : 0;
$email = isset($_POST['email']) && isset($_POST['email']) ? $_POST['email'] : 0;
$whatsapp = isset($_POST['whatsapp']) && isset($_POST['whatsapp']) ? $_POST['whatsapp'] : 0;


/* [FILE] só funciona com método [POST] definido no Formulário */
/* O [FILE] retorna as seguintes informações no [Array]
    [name] - Nome completo do arquivo com a extensão
    [type] - Tipo do arquivo. Caso seja enviado um arquivo PDF o tipo será [application/pdf], PNG [image/png], JPG [image/jpeg]
    [tmp_name] - é o local onde o arquivo fica salvo TEMPORÁRIAMENTE
    [error] - Retorna um código de erro caso tenha um problema de um erro
    [size] - Retorna o tamanho do arquivo   
*/
$cv = isset($_FILES['cv']) ? $_FILES['cv'] : 0;

/* MIME TYPE */
// Abaixo tenho uma pequena lista de MIME TYPE, no arquivo [mime-tipe.md] tem uma lista com mais.
// $pdf = "application/pdf";
// $docx = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
// $doc = "application/msword";
// $jpeg = "image/jpeg";
// $png = "image/png";
// $gif = "image/gif";

// Defino um Array com as extensões que serão aceitas
$cvExtensoes = [
    "application/pdf",
    "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
    "application/msword"
];

/* Faço uma condição verificando se as informações enviadas pelo formulário estão vazias, caso esteja mostre o erro abaixo. */
if ($nome == "" || $email == "" || $whatsapp == "" || $cv == "") {
    $msgErro = "<p class='erro'>Opss... Você precisa informar o <strong>Nome, E-mail, WhatsApp e CV</strong></p>";
} else {

    /* Aqui faço mais uma verificação das váriaveis, caso seja FALSE mostre o erro. */
    if ($nome != 0 && $email != 0 && $whatsapp != 0 && $cv != 0) {

        /* Verifico se o arquivo enviado é diferente de [PDF, DOC ou DOCX], caso seja, apresenta o erro. */
        // O [IF] comentado abaixo era a forma antiga.
        // if ($cv['type'] != $pdf && $cv['type'] != $docx && $cv['type'] != $doc) { 
        if (!in_array($cv['type'], $cvExtensoes)) {
            $msgErro = "<p class='erro'>Opss... Você deve enviar arquivos em <strong>PDF ou WORD!</strong></p>";
        } else {

            // Trato o nome do arquivo onde pego o nome original, coloco underlines e numeros
            // Ex do mb_strstr ou strstr em cima nos FUNÇÕES USADAS
            $cvNome = str_replace(mb_strstr($cv['name'], '.'), "", $cv['name']);
            $cvArquivo = $cvNome . "_" . time() . mb_strstr($cv['name'], '.');
            $cvArquivo = str_replace("-", "_", $cvArquivo);
            $cvArquivo = str_replace(" ", "_", $cvArquivo);

            /* ==== ESSE ERA A FORMA ANTIGA

             // Abaixo faço validações para saber a extensão do arquivo
            if ($cv['type'] == $pdf) {

                // Aqui faço uma alteração pegando o nome do arquivo, retiro o .pdf e depois implemento numeros para não repitir o mesmo nome, no final, coloco novamente o .pdf 
                $cvCandidato = str_replace(".pdf", "", $cv['name']) . "-" . rand(1, 999) . time() . ".pdf";
            } else if ($cv['type'] == $docx) {

                // Aqui faço uma alteração pegando o nome do arquivo, retiro o .docx e depois implemento numeros para não repitir o mesmo nome, no final, coloco novamente o .docx 
                $cvCandidato = str_replace(".docx", "", $cv['name']) . "-" . rand(1, 999) . time() . ".docx";
            } else {

                // Aqui faço uma alteração pegando o nome do arquivo, retiro o .doc e depois implemento numeros para não repitir o mesmo nome, no final, coloco novamente o .doc
                $cvCandidato = str_replace(".doc", "", $cv['name']) . "-" . rand(1, 999) . time() . ".doc";
            }
            */

            /* Verifico se o tamanho do arquivo e menor que 150KB e verifico se é o arquivo correto, caso seja, entra na condição. */
            if ($cv['size'] < 150000 && in_array($cv['type'], $cvExtensoes)) {

                // Verifico se o diretório já foi criado, caso não seja eu o crio.
                $pasta = __DIR__ . "/cv";

                /* A função [file_exists] é uma função integrada para verificar onde um diretório ou um arquivo existe ou não. Ela aceita um parâmetro de um caminho que retorna true se ele já existe ou false se não existir. */

                /* [is_dir] - Esta função também é semelhante a file_exists, e a única diferença é que ela só retornará true se a string passada for um diretório e retornará false se for um arquivo. */
                if (!file_exists($pasta) || !is_dir($pasta)) {

                    /* [PERMISSÂO DA PASTA] 
                    - 0600: Escrita e leitura para o proprietário, nada para os outros;
                    - 0644: Escrita e leitura para o proprietário, leitura para todos os outros;
                    - 0755: Tudo para o proprietário, leitura e execução para os outros;
                    - 0750: Tudo para o proprietário, leitura e execução para o grupo do proprietário.
                    */
                    mkdir($pasta, 0755);
                }

                // Com o [move_uploaded_file] eu movo o arquivo para a pasta [CV]
                move_uploaded_file($cv['tmp_name'], __DIR__ . "/cv/{$cvArquivo}");

                // Imprimo a mensagem de sucesso.
                $msgSucesso = "<h3>SUCESSO</h3>
                <p>Seu CV foi registra em nossa base, logo um recrutador entrará em contato.</p>";
            } else {
                $msgErro = "<p class='erro'>Opss... o tamanho não pode passar de <strong>150KB</strong></p>";
            }
        }
    }
}





?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Informe sua altura</title>
    <link rel="stylesheet" href="style/style.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>

<body>
    <section class="container">
        <div class="content-90-780">

            <h1>Envio de Arquivos <strong>PDF E DOC</strong></h1>
            <form action="" method="POST" enctype="multipart/form-data">
                <a href="?">Atualizar</a>

                <?= $msgErro; ?>

                <label class="box100"><strong>Seu Nome:</strong>
                    <input type="text" name="nome" placeholder="Ex: Francisco Assis" />
                </label>
                <label class="box100"><strong>Seu E-mail:</strong>
                    <input type="text" name="email" placeholder="Ex: francisco@teste.com.br" />
                </label>
                <label class="box100"><strong>Seu WhatsApp:</strong>
                    <input type="text" name="whatsapp" placeholder="Ex: (xx) xxxxx-xxxx" />
                </label>

                <label class="box100 send-file" id="label-file" title="Clique aqui para enviar seu CV"><strong>Seu CV:</strong>
                    <input type="file" name="cv" id="cv-files">
                    <span class="material-icons">
                        description
                    </span>
                </label>

                <div class="box100">
                    <input type="submit" value="Enviar" />
                </div>
                <div class="clear"></div>


                <?= $msgSucesso; ?>
            </form>

            <div class="clear"></div>
        </div>
    </section>

    <script>
        // Defino a variável
        let cv_file = document.getElementById('cv-files')

        // Crio a função de [onchange]
        cv_file.onchange = function() {
            // Pego o valor do campo input do formulário
            let cvNome = cv_file.value
            // Faço uma tratativo retirando o [C:\fakepath\]
            cvNome = cvNome.replace('C:\\fakepath\\', '')

            // Crio um elemento SPAN no DOM
            let arquivo_selecionado = document.createElement('span')
            // Adiciono uma classe chamada [arquivo_selecionado] ao SPAN
            arquivo_selecionado.classList.add('arquivo_selecionado')
            // Defino a cor da font
            arquivo_selecionado.style.color = "#555"
            // Envio uma mensagem com o [innerHTML]
            arquivo_selecionado.innerHTML = 'Arquivo <strong>' + cvNome + '</strong> selecionado'
            // Insiro todas as informações na [Label]
            document.getElementById('label-file').appendChild(arquivo_selecionado)

            // Verifico se a classe [arquivo_selecionado]
            if (document.getElementsByClassName('arquivo_selecionado')) {

                // Pego a classe caso ela exista
                removeClasse = document.querySelector('.arquivo_selecionado')
                // Removo a classe caso ela exista
                removeClasse.parentNode.removeChild(removeClasse);
            }
            // Insiro todas as informações na [Label] novamente
            document.getElementById('label-file').appendChild(arquivo_selecionado)
        }
    </script>

</body>

</html>