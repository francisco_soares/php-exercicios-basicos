<?php

// Fórmula: valorHora * quantHoras

/** [DICA]
 * ISSET: Verifica se a variável ou outro elemento existe. 
 * EMPTY: verifica se contém algum valor na variável (se ela esta vazia ou não).
 */

$msgErro = "";
$msgSucesso = "";

$valorHoras = isset($_GET['valorHora']) ? $_GET['valorHora'] : 0;
$horasTrabalhadas = isset($_GET['horasTrabalhadas']) ? $_GET['horasTrabalhadas'] : 0;

function paraReal($valor)
{
    return number_format($valor, 2, ",", '.');
}

if ($valorHoras == "" || $horasTrabalhadas == "") {
    $msgErro = "<p class='erro'>Opss... Você precisa informar o seu <strong>Valor por hora</strong> e <strong>Horas trabalhadas</strong></p>";
} else {

    $valorHoras = floatval($valorHoras);
    $horasTrabalhadas = floatval($horasTrabalhadas);

    if ($valorHoras > 0 && $horasTrabalhadas > 0) {

        $totalMes = $valorHoras * $horasTrabalhadas;
        $msgSucesso = "
                <h3>Resultado do mês:</h3>
                <p><strong>Seu valor por Hora:</strong> " . paraReal($valorHoras) . "</p>
                <p><strong>Horas trabalhadas:</strong> {$horasTrabalhadas}</p>
                <p><strong>Salário do mês:</strong> " . paraReal($totalMes) . "</p>
                ";
    }
}

?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Salário do Mês</title>
    <link rel="stylesheet" href="style/style.css">
</head>

<body>

    <section class="container">
        <div class="content-90-780">

            <h1>Salário do <strong>Mês</strong></h1>
            <form action="" method="GET">

                <a href="?">Atualizar</a>

                <?= $msgErro; ?>

                <label class="box100"><strong>Valor por hora:</strong>
                    <input type="text" name="valorHora" placeholder="Informe o seu valor por hora." />
                </label>

                <label class="box100"><strong>Quantidade de horas trabalhadas:</strong>
                    <input type="text" name="horasTrabalhadas" placeholder="Informe as suas horas trabalhadas." />
                </label>

                <div class="box100">
                    <input type="submit" value="Enviar" />
                </div>
                <div class="clear"></div>


                <?= $msgSucesso; ?>
            </form>

            <div class="clear"></div>
        </div>
    </section>

</body>

</html>