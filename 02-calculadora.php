<?php

$msgErro = "";
$msgSucesso = "";

$valor1 = isset($_GET['valor1']) ? $_GET['valor1'] : 'nula';
$valor2 = isset($_GET['valor2']) ? $_GET['valor2'] : 'nula';
$operador = isset($_GET['operador']) ? $_GET['operador'] : 'nula';

if ($valor1 == "" || $valor2 == "") {
    $msgErro = "<p class='erro'>Opss... Você precisa informar as <strong>os numeros.</strong></p>";
} else {
    $valor1 = floatval($valor1);
    $valor2 = floatval($valor2);
    $total = '';

    if ($operador == "som" || $operador == "sub" || $operador == "mul" || $operador == "div") {
        if ($operador == "som") {
            $total = $valor1 + $valor2;
            $msgSucesso = "
        <h3>Resultado da operação</h3>
        A soma dos números <strong>{$valor1}</strong> e <strong>{$valor2}</strong>  é igual á: <strong>{$total}</strong>.
        ";
        } elseif ($operador == "sub") {
            $total = $valor1 - $valor2;
            $msgSucesso = "
        <h3>Resultado da operação</h3>
        A subtração dos números <strong>{$valor1}</strong> e <strong>{$valor2}</strong>  é igual á: <strong>{$total}</strong>.
        ";
        } elseif ($operador == "mul") {
            $total = $valor1 * $valor2;
            $msgSucesso = "
        <h3>Resultado da operação</h3>
        A multiplicação dos números <strong>{$valor1}</strong> e <strong>{$valor2}</strong>  é igual á: <strong>{$total}</strong>.
        ";
        } else {
            $total = $valor1 / $valor2;
            $msgSucesso = "
        <h3>Resultado da operação</h3>
        A divisão dos números <strong>{$valor1}</strong> e <strong>{$valor2}</strong>  é igual á: <strong>{$total}</strong>.
        ";
        }
    }
}
// if($valor1 != "" && $valor2 != ""){

// }else{
//     
// }

?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Média do Aluno</title>
    <link rel="stylesheet" href="style/style.css">
</head>

<body>

    <section class="container">
        <div class="content-90-780">

            <h1>Calcu<strong>ladora</strong></h1>
            <form action="" method="GET">

                <a href="?">Atualizar</a>

                <?= $msgErro; ?>

                <label class="box100"><strong>Valor 1</strong>
                    <input type="text" name="valor1" placeholder="Informe o 1º valor." />
                </label>

                <label class="box100"><strong>Operador</strong>
                    <select name="operador">
                        <option value="som">+</option>
                        <option value="sub">-</option>
                        <option value="mul">*</option>
                        <option value="div">/</option>
                    </select>
                </label>

                <label class="box100"><strong>Valor 2</strong>
                    <input type="text" name="valor2" placeholder="Informe o 2º valor." />
                </label>

                <div class="box100">
                    <input type="submit" value="Calcular" />
                </div>
                <div class="clear"></div>


                <?= $msgSucesso; ?>
            </form>

            <div class="clear"></div>
        </div>
    </section>

</body>

</html>