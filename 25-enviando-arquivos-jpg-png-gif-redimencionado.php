<?php

/**
 * EXERCÍCIO:
 * 1º - Crie um formulário com o campo: Nome da imagem e Enviar imagem
 *    - Os campos devem ser validados
 *    - O campo deve aceitar as imagens com as seguintes extensões: JPG, JPEG, PNG e GIF
 */

/**
 * FUNÇÕES USADAS
 * [str_replace]: https://www.w3schools.com/php/func_string_str_replace.asp
 * 
 * *********************
 * 
 * [mb_strstr ou strstr]: https://www.php.net/manual/pt_BR/function.mb-strstr.php
 * - Encontra a primeira ocorrência de uma string dentro de outra.
 * Ex: 
 * $img = logotipo.jpg
 * mb_strstr($img, '.')
 * Será pego a [STRING] da variável a partir do ponto, então fica assim: (.jpg)
 * 
 * *********************
 * 
 * [time]: https://www.php.net/manual/pt_BR/function.time.php
 * - Retorna o timestamp Unix atual
 * Ex: var_dump(time())
 * 
 * *********************
 * 
 * [in_array]: https://www.php.net/manual/pt_BR/function.in-array.php
 * - Checa se um valor existe em um array
 * Ex:
 * $os = array("Mac", "NT", "Irix", "Linux"); 
 * $teste = in_array("Irix", $os) - a variável $teste receberá o valor de TRUE
 * 
 * *********************
 * 
 * [file_exists]: https://www.php.net/manual/pt_BR/function.file-exists.php
 * É uma função integrada para verificar onde um diretório ou um arquivo existe ou não. Ela aceita um parâmetro de um caminho que retorna true se ele já existe ou false se não existir.
 * 
 * *********************
 * 
 * [is_dir]: https://www.php.net/manual/pt_BR/function.is-dir.php
 * Esta função também é semelhante a file_exists, e a única diferença é que ela só retornará true se a string passada for um diretório e retornará false se for um arquivo.
 * 
 * *********************
 * 
 * [mkdir]: https://www.php.net/manual/pt_BR/function.mkdir.php
 * Cria um diretório
 * 
 * *********************
 * 
 * [move_uploaded_file]
 * 
 */

$msg_erro = "";
$msg_sucesso = "";

// Antes de pegar os dados enviado via Formulário, eu faço uma validação para verificar se realmente foi enviado os dados.
$nome_arquivo = (isset($_POST['nome_arquivo']) && !empty($_POST['nome_arquivo'])) ? $_POST['nome_arquivo'] : 0;

/* [FILE] só funciona com método [POST] definido no Formulário */
/* O [FILE] retorna as seguintes informações no [Array]
    [name] - Nome completo do arquivo com a extensão
    [type] - Tipo do arquivo. Caso seja enviado um arquivo PDF o tipo será [application/pdf], PNG [image/png], JPG [image/jpeg]
    [tmp_name] - é o local onde o arquivo fica salvo TEMPORÁRIAMENTE
    [error] - Retorna um código de erro caso tenha um problema de um erro
    [size] - Retorna o tamanho do arquivo   
*/
$img = (isset($_FILES['imagem']) && !empty($_FILES['imagem'])) ? $_FILES['imagem'] : 0;


function enviarLogoTipo($img_mime_name, $img_mime_type, $img_mime_size, $img_mime_tmp_name)
{
    // Defino um Array com as extensões que serão aceitas
    $arr_img = [
        "image/jpeg",
        "image/png",
        "image/gif"
    ];

    // Defino o tamanho do redimencionamento da imagem
    $img_tamanho = "90";

    // Retiro espços em branco e hífes e substituo por underline e deixo tudo em caixa baixa além de retirar caracteres especiais
    $img_arquivo = str_replace(strstr($img_mime_name, '.'), "", $img_mime_name);

    // Pego o nome passado na váriavel [$nome_arquivo] e concateno com [.extensao], já que na função [mb_strstr] estou pedindo para pegar
    // do arquivo [$img['name']] a partir do [.] para frente.
    $img_logo = $img_arquivo . "_" . time() . mb_strstr($img_mime_name, '.');

    // Aqui faço uma tratativa para retirar caracteres especiais
    $com_acentos = 'ÀÁÂÃÄÅÆÇçÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜüÝÞßàáâãäåæèéêëìíîïðñòóôõöøùúûýýþÿR"!@#$%&*()-+={[}]/?;:,\\\'<>°ºª~^´`';
    $sub_sem_acento = 'aaaaaaacceeeeiiiidnoooooouuuuuybsaaaaaaaeeeeiiiidnoooooouuuyybyR_______________________________';
    $img_logo = strtr(utf8_decode($img_logo), utf8_decode($com_acentos), $sub_sem_acento);

    // Abaixo retiro os traços e substituo pelo underline
    $img_logo = str_replace('-', '_', $img_logo);
    // Abaixo retiro os espaços em branco e substituo po underline
    $img_logo = str_replace(' ', '_', $img_logo);
    // Abaixo deixo o nome em minusculo.
    $img_logo = strtolower($img_logo);

    /* Verifico se o tamanho do arquivo e menor que 100KB e verifico se é o arquivo correto, caso seja, entra na condição. */
    if ($img_mime_size < 10000000 && in_array($img_mime_type, $arr_img)) {

        // Verifico se o diretório já foi criado, caso não seja eu o crio.
        $pasta = __DIR__ . "/img";

        /* A função [file_exists] é uma função integrada para verificar onde um diretório ou um arquivo existe ou não. Ela aceita um parâmetro de um caminho que retorna true se ele já existe ou false se não existir. */

        /* [is_dir] - Esta função também é semelhante a file_exists, e a única diferença é que ela só retornará true se a string passada for um diretório e retornará false se for um arquivo. */
        if (!file_exists($pasta) || !is_dir($pasta)) {
            /* [PERMISSÂO DA PASTA] 
                    - 0600: Escrita e leitura para o proprietário, nada para os outros;
                    - 0644: Escrita e leitura para o proprietário, leitura para todos os outros;
                    - 0755: Tudo para o proprietário, leitura e execução para os outros;
                    - 0750: Tudo para o proprietário, leitura e execução para o grupo do proprietário.
                    */
            mkdir($pasta, 0755);
        }

        // Verifico o tipo para criar uma imagem temporária do formato especifico
        if ($img_mime_type === "image/jpeg") {
            $img_temporaria = imagecreatefromjpeg($img_mime_tmp_name);
        } else if ($img_mime_type === "image/png") {
            $img_temporaria = imagecreatefrompng($img_mime_tmp_name);
        } else {
            $img_temporaria = imagecreatefromgif($img_mime_tmp_name);
        }

        var_dump($img_temporaria);
        die;

        // Pego a largura e altura da imagem original
        $largura_original = imagesx($img_temporaria);
        $altura_original = imagesy($img_temporaria);

        // Crio a nova largura e altura para 90px
        $nova_largura = $img_tamanho ? $img_tamanho : floor(($largura_original / $altura_original) * $img_tamanho);
        $nova_altura = $img_tamanho ? $img_tamanho : floor(($altura_original / $altura_original) * $img_tamanho);

        // Retorna a imagem com largura e altura com 90px
        $img_redimencionada = imagecreatetruecolor($nova_largura, $nova_altura);

        if ($img_mime_type === "image/gif") {
            //Trata transparência da extensão GIF
            $imgGifTransparente = imagecolorallocate($img_redimencionada, 255, 255, 255);
            imagecolortransparent($img_redimencionada, $imgGifTransparente);
            imagefill($img_redimencionada, 0, 0, $imgGifTransparente);
        } else if ($img_mime_type === "image/png") {
            //Trata transparência da extensão PNG
            imagealphablending($img_redimencionada, false);
            imagesavealpha($img_redimencionada, true);
            imagecolortransparent($img_redimencionada);
        }

        // Copio a imagem para a nova imagem
        imagecopyresampled($img_redimencionada, $img_temporaria, 0, 0, 0, 0, $nova_largura, $nova_altura, $largura_original, $altura_original);

        // Verifico o tipo da imagem para salvar com sua determinada extensão.
        if ($img_mime_type === "image/jpeg") {
            imagejpeg($img_redimencionada, "img/{$img_logo}");
        } else if ($img_mime_type === "image/png") {
            imagepng($img_redimencionada, "img/{$img_logo}");
        } else {
            imagegif($img_redimencionada, "img/{$img_logo}");
        }

        // Imprimo a mensagem de sucesso.
        return "
            <h3>SUCESSO</h3>
            <p>Imagem enviada com sucesso.</p>
            ";
    } else {
        return $msg_erro = "<p class='erro'>Opss... o tamanho não pode passar de <strong>100KB</strong></p>";
    }
}

if($img && $nome_arquivo){
    $msg_sucesso = enviarLogoTipo($img['name'], $img['type'], $img['size'], $img['tmp_name']);
}

?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Envio de Arquivos JPG, PNG e GIF</title>
    <link rel="stylesheet" href="style/style.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>

<body>
    <section class="container">
        <div class="content-90-780">

            <h1>Envio de Arquivos <strong>JPG, PNG e GIF</strong></h1>
            <form action="" method="POST" enctype="multipart/form-data">
                <a href="?">Atualizar</a>

                <?= $msg_erro; ?>

                <label class="box100"><strong>Nome imagem:</strong>
                    <input type="text" name="nome_arquivo" placeholder="Ex: foto no parque" />
                </label>

                <label class="box100 send-file" id="label_file" title="Clique aqui para enviar seu CV"><strong>Enviar imagem:</strong>
                    <input type="file" name="imagem" id="img_file">
                    <span class="material-icons">image</span>
                </label>

                <div class="box100">
                    <input type="submit" value="Enviar" />
                </div>
                <div class="clear"></div>

                <?= $msg_sucesso; ?>
            </form>

            <div class="clear"></div>
        </div>
    </section>

    <!-- Script de alteração do campo FILE -->
    <script>
        // Pego o ID co campo [img_file]
        let img = document.getElementById('img_file')

        img.onchange = function() {
            // Pego o valor que vem da variavel [img]
            let imgFile = img.value
            // Retiro o [C:\\fakepath\\] do caminho que apresenta quando inserido o arquivo
            imgFile = imgFile.replace('C:\\fakepath\\', '')

            // Crio o elemento [SPAN] no DOM
            arquivoSelecionado = document.createElement('span')
            // Crio a classe [arquivo_selecionado] dentro do [SPAN]
            arquivoSelecionado.classList.add('arquivo_selecionado')
            // Formato a cor do texto para uma cor cinza
            arquivoSelecionado.style.color = "#555"
            // Inserido o texto dentro do [SPAN]
            arquivoSelecionado.innerHTML = "Arquivo <strong>" + imgFile + "</strong> selecionado."

            // Insiro o [SPAN] dentro da [label_file]
            document.getElementById('label_file').appendChild(arquivoSelecionado)

            // Verifico se a classe [arquivo_selecionado] existe dentro do DOM.
            if (document.getElementsByClassName('arquivo_selecionado')) {
                // Selecio a classe [arquivo_selecionado]
                removeClasse = document.querySelector('.arquivo_selecionado')
                // Removo a classe [arquivo_selecionado]
                removeClasse.parentNode.removeChild(removeClasse);
            }
            // Insiro novamente o [SPAN] dentro da [label_file]
            document.getElementById('label_file').appendChild(arquivoSelecionado)
        }
    </script>

</body>

</html>