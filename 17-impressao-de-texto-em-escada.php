<?php

/**
 * EXERCÍCIO:
 * Criar um algoritmos que entre com uma palavra e imprima conforme o exemplo: Palavra: sonho
 * SONHO
 * SONHO SONHO
 * SONHO SONHO SONHO
 * SONHO SONHO SONHO SONHO
 */
$msgErro = "";
$msgSucesso = "";

// 

$texto = isset($_GET['texto']) ? $_GET['texto'] : 'qwer';


// if (isset($_GET['texto'])) {
//     if (isset($_GET['texto']) != "") {
//         for ($i = 1; $i <= 1; $i++) {
//             echo strtoupper($texto) . "<br>";
//             for ($j = 1; $j <= 2; $j++) {
//                 echo strtoupper($texto) . " ";
//             }
//         }
//     } else {
//         $msgErro = "<p class='erro'>Opss... Você precisa informar uma <strong>Palavra</strong>.</p>";
//     }
// }
if ($texto == "") {
    $msgErro = "<p class='erro'>Opss... Você precisa informar uma <strong>Palavra</strong>.</p>";
} else {

    if ($texto != "qwer") {
        $msgSucesso = "<h3>Resultado</h3>";
        for ($i = 1; $i <= 1; $i++) {
            $msgSucesso .= strtoupper($texto) . "<br>";

            for ($j = 1; $j <= 2; $j++) {
                $msgSucesso .= strtoupper($texto) . " ";
                if ($j == 2) {
                    $msgSucesso .= "<br>";

                    for ($z = 1; $z <= 3; $z++) {
                        $msgSucesso .= strtoupper($texto) . " ";

                        if ($z == 3) {
                            $msgSucesso .= "<br>";

                            for ($x = 1; $x <= 4; $x++) {
                                $msgSucesso .= strtoupper($texto) . " ";

                                if ($x == 4) {
                                    $msgSucesso .= "<br>";
                                    for ($q = 1; $q <= 5; $q++) {
                                        $msgSucesso .= strtoupper($texto) . " ";
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Impressão de texto em escada</title>
    <link rel="stylesheet" href="style/style.css">
</head>

<body>

    <section class="container">
        <div class="content-90-780">

            <h1>Impressão de texto <strong>em escada</strong></h1>
            <form action="" method="GET">

                <a href="?">Atualizar</a>

                <?= $msgErro; ?>

                <label class="box100"><strong>Palavra:</strong>
                    <input type="text" name="texto" placeholder="Informe apenas uma palavra:" />
                </label>

                <div class="box100">
                    <input type="submit" value="Enviar" />
                </div>
                <div class="clear"></div>


                <?= $msgSucesso; ?>
            </form>

            <div class="clear"></div>
        </div>
    </section>

</body>

</html>