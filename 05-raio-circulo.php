<?php

// PERIMETRO: 2 * M_PI ou pi() * raio-digitado | ÁREA: M_PI ou pi() * raio-digita * radio-digitado

$msgErro = "";
$msgSucesso = "";

$raio = isset($_GET['raio']) ? $_GET['raio'] : 0;

if ($raio == '') {
    $msgErro = "<p class='erro'>Opss... Você precisa informar o <strong>Raio.</strong></p>";
} else {

    $raio = floatval($raio);

    if ($raio > 0) {
        $perimetro = (2 * pi()) * $raio;
        $area = pi() * $raio * $raio;

        $msgSucesso = "
            <h3>Resultado:</h3>
            <p><strong>RAIO INFORMADO:</strong> {$raio}</p>
            <p><strong>PERIMETRO DO RAIO:</strong> {$perimetro}</p>
            <p><strong>ÁREA DO RAIO</strong> {$area}</p>
        ";
    }
}


?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Raio de um Circulo</title>
    <link rel="stylesheet" href="style/style.css">
</head>

<body>

    <section class="container">
        <div class="content-90-780">

            <h1>Raio de um <strong>Circulo</strong></h1>
            <form action="" method="GET">

                <a href="?">Atualizar</a>

                <?= $msgErro; ?>

                <label class="box100"><strong>Informe o Raio do circulo</strong>
                    <input type="text" name="raio" placeholder="Informe o raio do circulo." />
                </label>

                <div class="box100">
                    <input type="submit" value="Enviar" />
                </div>
                <div class="clear"></div>


                <?= $msgSucesso; ?>
            </form>

            <div class="clear"></div>
        </div>
    </section>

</body>

</html>