# Lista de exercícios para estimular a lógica
*Todo os exercícios foram tirados da internet.*<br><br>

**AVISO:** Sempre criar a BRANCH com o nome do exercicio, Ex: *01-exercicio*<br>
Após terminar, faça o teste antes de enviar para a **BRANCH MASTER**.<br><br>


**1º** - Faça um script que peça 3 notas de um aluno e mostra sua média.<br><br>

**2º** - Crie uma calculadora onde faça a SOMA, SUBTRAÇÃO, DIVISÃO e MULTIPLICAÇÃO<br><br>

**3º** - Criar uma tabuada onde você informa o operador de multiplicação e até quanto quanto deve multiplicar<br><br>

**4º** - Faça um script que converta metros para centímetros.<br>
> Fórmula: metro x 100 = centimetros


**5º** - Escreva um script que pede o raio de um círculo, e em seguida exiba o perímetro e área do círculo.<br>
> Fórmula:  2 * M_PI ou pi() * *raio-digitado*<br>
> Sobre a função pi() [link](https://www.php.net/manual/pt_BR/function.pi.php)

**6º** - Faça um script que calcule a área de um quadrado, em seguida mostre o dobro desta área para o usuário.<br><br>
> Fórmula:  Área * 2<br>

**7º** - Faça um script que peça a temperatura em graus Farenheit, transforme e mostre a temperatura em graus Celsius.<br>
> Fórmula: C = (5 * (<valor>-32) / 9)<br>

**8º** - Faça um script que peça a temperatura em graus Celsius, transforme e mostre em graus Farenheit.<br>
> Fórmula: (<valor> °C × 9/5) + 32 = 50 °F<br>

**9º** - Faça um script que pergunte quanto você ganha por hora e o número de horas trabalhadas no mês. Calcule e mostre o total do seu salário no referido mês.<br>

**10º** - Tendo como dados de entrada a altura de uma pessoa, construa um script que calcule seu peso ideal, usando a seguinte.<br> 
> Fórmula: (72.7*altura) - 58<br>

**11º** - Tendo como dado de entrada a altura(h) de uma pessoa, construa um script que calcule seu peso ideal, utilizando as seguintes.<br> 
> Fórmulas: Para homens: (72.7*h) - 58 | Para mulheres: (62.1*h) - 44.7<br>

**12º** - João Papo-de-Pescador, homem de bem, comprou um microcomputador para controlar o rendimento diário de seu trabalho. Toda vez que ele traz um peso de peixes maior que o estabelecido pelo regulamento de pesca do estado de São Paulo (50 quilos) deve pagar uma multa de R$ 4,00 por quilo excedente. João precisa que você faça um script que leia a variável peso (peso de peixes) e calcule o excesso. Gravar na variável excesso a quantidade de quilos além do limite e na variável multa o valor da multa que João deverá pagar. Imprima os dados do script com as mensagens adequadas.<br><br>

**13º** - Faça um script que pergunte quanto você ganha por hora e o número de horas trabalhadas no mês. Calcule e mostre o total do seu salário no referido mês, sabendo-se que são descontados 11% para o Imposto de Renda, 8% para o INSS e 5% para o sindicato, faça um script que nos dê: Salário Bruto : R$ IR (11%) : R$ INSS (8%) : R$ Sindicato ( 5%) : R$ Salário Liquido : R$ Obs.: Salário Bruto - Descontos = Salário Líquido.<br><br>

**14º** - Construir um algoritmo que leia 2 números e efetue a adição. Caso o valor somado seja maior que 20, este deverá ser presentando somando-se a ele mais 8; caso o valor somado seja menor ou igual a 20, este deverá ser apresentado subtraindo-se 5.<br><br>

**15º** - Entrar com um número e informar se ele é divisível por 10, por 5, por 2 ou se não é divisível por nenhum destes<br><br>

**16º** - Ler um número inteiro entre 1 e 12 e escrever o mês correspondente. Caso o número seja fora desse intervalo, informar que não existe mês com este número<br><br>

**17º** - A biblioteca de uma universidade deseja fazer um algoritmo que leia o nome do livro que será emprestado, o tipo de usuário (professor ou aluno) e possa imprimir um recibo conforme mostrado a seguir. Considerar que o professor tem 10 dias para devolver o livro o aluno somente 3 dias<br><br>

**18º** - Criar um algoritmos que entre com uma palavra e imprima conforme o exemplo: Palavra: sonho<br> 
> SONHO<br> 
> SONHO SONHO<br> 
> SONHO SONHO SONHO<br> 
> SONHO SONHO SONHO SONHO<br>

**19º** - Criar um arquivo em XML e Ler esse arquivo.<br><br>

**20º** - Faça um script para uma loja de tintas. O script deverá pedir o tamanho em metros quadrados da área a ser pintada. Considere que a cobertura da tinta é de 1 litro para cada 3 metros quadrados e que a tinta é vendida em latas de 18 litros, que custam R$ 80,00. Informe ao usuário a quantidades de latas de tinta a serem compradas e o preço total.<br><br>

**21º** - Ler vários números e informar quantos números entre 100 e 200 foram digitados. Se o valor 0 for lido encerrar a execução<br><br>

**22º** - Chico tem 1,50m e cresce 2 centímetros por ano, enquanto Juca tem 1,10m e cresce 3 centímetros por ano. Construir um algoritmos que calcule e imprima quantos anos serão necessários para que Juca seja maior que Chico<br><br>