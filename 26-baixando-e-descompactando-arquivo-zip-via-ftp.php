<?php

/**
 * EXERCÍCIO:
 * Crie um script que baixe um arquivo zip e extraia via ftp.
 */

// Acesso FTP
// $ftp_server = "192.185.218.160";
$ftp_server = "srv42.prodns.com.br";
// Faço a conexão e envio para a váriavel $ftp_conn
$ftp_conn = ftp_connect($ftp_server) or die("Could not connect to {$ftp_server}");

// Faço o login com a função ftp_login, passando os parametros FTP, USUARIO, SENHA
// $login = ftp_login($ftp_conn, "webfrc88", "797birdblack");
$login = ftp_login($ftp_conn, "anonymous@webfr.com.br", "");

// Defino as váriaveis
// $local_file: defino o nome do arquivo quando baixado para meu PC
// $server_file: Busco o arquivo para download
$local_file = 'local.zip';
$server_file = 'server.zip';

// Entro na condição verificando se o arquivo .zip do servidor foi baixo e salvo em meu PC
if (ftp_get($ftp_conn, $local_file, $server_file, FTP_BINARY)) {

    echo "Arquivo $local_file baixado com sucesso<br>";

    // Instancio a classe ZipArchive
    $zip = new ZipArchive;

    // Entro no laço para verificar se o arquivo existe dentro do PC
    if ($zip->open($local_file) === true) {

        // Extraio o arquivo caso ele exista para dentro da pasta img
        $zip->extractTo('img');
        // Fecho o método de extração
        $zip->close();
        echo "Aquivo extraido";
    }
} else {
    echo "Há algum problema.";
}

// Fecho a conexão com o servidor.
ftp_close($ftp_conn);
