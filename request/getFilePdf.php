<?php

$jSON = [];

if (isset($_POST) && !empty($_POST)) {
    $mes = $_POST['mes'];
    $ano = $_POST['ano'];

    $dir = "../pdf/" . $ano . "/" . $mes . "/";



    if (is_dir($dir)) {
        $files = array_diff(scandir($dir), ['.', '..']);
        $files = array_values($files);

        if (!empty($files)) {
            $jSON["dir"] = $dir;
            $jSON["files"] = $files;
            $jSON["success"] = true;
        } else {
            $jSON['error'] = true;
        }
    } else {
        $jSON['error'] = true;
    }
} else {
    $jSON['error'] = true;
}
echo json_encode($jSON);
