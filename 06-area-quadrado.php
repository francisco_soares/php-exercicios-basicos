<?php

// Fórmula:  CALCULO ÁREA: Área * Área | DOBRO ÁREA: Área * 2

$msgErro = "";
$msgSucesso = "";

$quadrado = isset($_GET['quadrado']) ? $_GET['quadrado'] : 0;

if ($quadrado == "") {
    $msgErro = "<p class='erro'>Opss... Você precisa informar a <strong>Área do quadrado.</strong></p>";
} else {

    $quadrado = floatval($quadrado);

    if ($quadrado > 0) {
        $area = $quadrado * $quadrado;
        $dobro = $area * 2;

        $msgSucesso = "
            <h3>Resultado:</h3>
            <p><strong>Área do quadrado:</strong> {$area}</p>
            <p><strong>Dobro da Área:</strong> {$dobro}</p>
        ";
    }
}


?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Área do quadrado</title>
    <link rel="stylesheet" href="style/style.css">
</head>

<body>

    <section class="container">
        <div class="content-90-780">

            <h1>Área do <strong>Quadrado</strong></h1>
            <form action="" method="GET">

                <a href="?">Atualizar</a>

                <?= $msgErro; ?>

                <label class="box100"><strong>Área do quadrado:</strong>
                    <input type="text" name="quadrado" placeholder="Informe o Área do quadrado" />
                </label>

                <div class="box100">
                    <input type="submit" value="Enviar" />
                </div>
                <div class="clear"></div>


                <?= $msgSucesso; ?>
            </form>

            <div class="clear"></div>
        </div>
    </section>

</body>

</html>