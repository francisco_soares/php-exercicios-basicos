<?php

/**
 * EXERCÍCIO:
 * Ler vários números e informar quantos números entre 100 e 200 foram digitados. Se o valor 0 for lido encerrar a execução
 */
$msgErro = "";
$msgSucesso = "";

// 

$numeros = isset($_GET['numeros']) ? $_GET['numeros'] : -1;
$seguranca = isset($_GET['seguranca']) ? $_GET['seguranca'] : 0;

$numArr = explode(', ', $numeros);
$numArr = array_map('trim', $numArr);
$arrCount = count($numArr);

if (in_array('0', $numArr)) {
    $msgErro = "<p class='erro'>Você digitou o <strong>numero 0</strong> em algum momento, sendo assim a aplicação foi encerrada automáticamente!</p>";
} else {
    if ($seguranca > 0) {
        $msgSucesso = "
            <h3>Resultado:</h3>
            <p>Você digitou os seguintes números<br> <strong>{$numeros}</strong></p><br>
            <p>Entre esses números, <strong>{$arrCount}</strong> estão entre <strong>100 e 200</strong></p>
        ";

        foreach($numArr as $num){
            $msgSucesso .= "| {$num} ";
        }
    }
}

?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Leitura de numeros</title>
    <link rel="stylesheet" href="style/style.css">
</head>

<body>

    <section class="container">
        <div class="content-90-780">

            <h1>Leitura de <strong>Números</strong></h1>
            <form action="" method="GET">

                <a href="?">Atualizar</a>

                <?= $msgErro; ?>

                <label class="box100"><strong>Números:</strong>
                    <input type="text" name="numeros" placeholder="Ex: 10, 150, 120, 0" />
                    <input type="hidden" name="seguranca" value="1" />
                </label>

                <div class="box100">
                    <input type="submit" value="Enviar" />
                </div>
                <div class="clear"></div>


                <?= $msgSucesso; ?>
            </form>

            <div class="clear"></div>
        </div>
    </section>

</body>

</html>