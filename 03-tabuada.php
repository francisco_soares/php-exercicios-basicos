<?php

$msgErro = "";
$msgSucesso = "";

$mult = isset($_GET['mult']) ? $_GET['mult'] : "nula";
$ate = isset($_GET['ate']) ? $_GET['ate'] : "nula";

if ($mult == "" || $ate == "") {
    $msgErro = "<p class='erro'>Opss... Você precisa informar o <strong>número MULTIPLICADOR e ATÉ QUANTO deve ir.</strong></p>";
} else {

    $mult = intval($mult);
    $ate = intval($ate);

    if (is_int($mult) && is_int($ate)) {
        if ($mult > -0.1 && $ate > 0) {
            $msgSucesso = "<h3>Resultado da tabuada:</h3>";
            for ($i = 1; $i <= $ate; $i++) {
                $msgSucesso .= "<strong>{$mult} x {$i}</strong> = " . $mult * $i . "<br>";
            }
        }
    }
}


?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tabuada</title>
    <link rel="stylesheet" href="style/style.css">
</head>

<body>

    <section class="container">
        <div class="content-90-780">

            <h1>Tabu<strong>ada</strong></h1>
            <form action="" method="GET">

                <a href="?">Atualizar</a>

                <?= $msgErro; ?>

                <label class="box100"><strong>Multiplicador</strong>
                    <input type="text" name="mult" placeholder="Informe o número Multiplicador." />
                </label>

                <label class="box100"><strong>Até quanto?</strong>
                    <input type="text" name="ate" placeholder="Ex: 15" />
                </label>

                <div class="box100">
                    <input type="submit" value="Calcular" />
                </div>
                <div class="clear"></div>


                <?= $msgSucesso; ?>
            </form>

            <div class="clear"></div>
        </div>
    </section>

</body>

</html>