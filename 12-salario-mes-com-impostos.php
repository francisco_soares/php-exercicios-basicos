<?php

/**
* FORMULAS:
* IR: 11 * salario bruto / 100;
* INSS: 8 * salario bruto / 100;
* Sindicato: 5 * salario bruto / 100;
* Salário Líquido: salário bruto - Descontos;
**/

$msgErro = "";
$msgSucesso = "";

$valorHora = isset($_GET['valorHora']) ? $_GET['valorHora'] : 0;
$horasTrabalhadas = isset($_GET['horasTrabalhadas']) ? $_GET['horasTrabalhadas'] : 0;

function real($valor){
    return number_format($valor, 2, ",", ".");
}

if ($valorHora == '' || $horasTrabalhadas == '') {
    $msgErro = "<p class='erro'>Opss... Você precisa informar o seu <strong>Valor por hora</strong> e <strong>Horas trabalhadas</strong></p>";
} else {

    $valorHora = floatval($valorHora);
    $horasTrabalhadas = floatval($horasTrabalhadas);

    if($valorHora > 0 && $horasTrabalhadas > 0){
        $salarioBruto = $horasTrabalhadas * $valorHora;
        $ir = 11 * $salarioBruto / 100;
        $inss = 8 * $salarioBruto / 100;
        $sindicato = 5 * $salarioBruto / 100;
        $totalDesconto = $ir + $inss + $sindicato;
        $salarioLiquido = $salarioBruto - $totalDesconto;

        $msgSucesso = "
        <h3>Resultado:</h3>
        <p><strong>Salário Bruto:</strong> R$ " . real($salarioBruto) . "</p>        
        <p><strong>IR:</strong> R$ " . real($ir) . "</p>        
        <p><strong>INSS:</strong> R$ " . real($inss) . "</p>        
        <p><strong>Sindicato:</strong> R$ " . real($sindicato) . "</p>        
        <p><strong>Total de Desconto:</strong> R$ " . real($totalDesconto) . "</p>        
        <p><strong>Salário Líquido:</strong> R$ " . real($salarioLiquido) . "</p>                
        ";
    }
}

?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Salário do Mês com impostos</title>
    <link rel="stylesheet" href="style/style.css">
</head>

<body>

    <section class="container">
        <div class="content-90-780">

            <h1>Salário do <strong>Mês com Impostos</strong></h1>
            <form action="" method="GET">

                <a href="?">Atualizar</a>

                <?= $msgErro; ?>

                <label class="box100"><strong>Valor por hora:</strong>
                    <input type="text" name="valorHora" placeholder="Informe o seu valor por hora." />
                </label>

                <label class="box100"><strong>Quantidade de horas trabalhadas:</strong>
                    <input type="text" name="horasTrabalhadas" placeholder="Informe as suas horas trabalhadas." />
                </label>

                <div class="box100">
                    <input type="submit" value="Enviar" />
                </div>
                <div class="clear"></div>


                <?= $msgSucesso; ?>
            </form>

            <div class="clear"></div>
        </div>
    </section>

</body>

</html>