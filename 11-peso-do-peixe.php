<?php
$msgErro = "";
$msgSucesso = "";

function real($valor)
{
    return number_format($valor, 2, ",", '.');
}

$pesoPeixe = isset($_GET['pesoPeixe']) ? $_GET['pesoPeixe'] : 0;

if ($pesoPeixe == "") {
    $msgErro = "<p class='erro'>Opss... Você precisa informar o <strong>PESO</strong> do <strong>PEIXE</strong></p>";
} else {

    $pesoPeixe = floatval($pesoPeixe);

    if ($pesoPeixe > 0) {
        $pesoRegulamento = 50.00;
        $valorPorKiloExcedido = 4.00;
        $excesso = $pesoPeixe > $pesoRegulamento ? $pesoPeixe - $pesoRegulamento : 0;
        $multa = $excesso * $valorPorKiloExcedido;

        $msgSucesso = "
        <h3>Resultado:</h3>
        <p><strong>Peso do seu Peixe:</strong> {$pesoPeixe}kg</p>
        <p><strong>Peso do Regulamento:</strong> {$pesoRegulamento}kg</p>
        <p><strong>Valor por Kilo excedido:</strong> R$ " . real($valorPorKiloExcedido) . "</p>
        <p><strong>Kilo excedido:</strong> {$excesso}kg</p>        
        <p><strong>Valor da Multa:</strong> R$ " . real($multa) . "</p>        
        ";
    }
}


?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Peso do Peixe</title>
    <link rel="stylesheet" href="style/style.css">
</head>

<body>

    <section class="container">
        <div class="content-90-780">

            <h1>Peso do <strong>Peixe</strong></h1>
            <form action="" method="GET">

                <a href="?">Atualizar</a>

                <?= $msgErro; ?>

                <label class="box100"><strong>Peso do peixe:</strong>
                    <input type="text" name="pesoPeixe" placeholder="Informe o peso do peixe." />
                </label>

                <div class="box100">
                    <input type="submit" value="Enviar" />
                </div>
                <div class="clear"></div>

                <?= $msgSucesso; ?>
            </form>

            <div class="clear"></div>
        </div>
    </section>

</body>

</html>