<?php

// Fórmula: C = ([valor] - 32) * 5 / 9)

/** [DICA]
 * ISSET: Verifica se a variável ou outro elemento existe. 
 * EMPTY: verifica se contém algum valor na variável (se ela esta vazia ou não).
 */

$msgErro = "";
$msgSucesso = "";

$grau = isset($_GET['grau']) ? $_GET['grau'] : -0.01;

if ($grau == '') {
    $msgErro = "<p class='erro'>Opss... Você precisa informar o <strong>Grau.</strong></p>";
} else {
    $grau = floatval($grau);

    if ($grau > -0.01) {
        $conversao = ($grau - 32) * 5 / 9;

        $msgSucesso = "
        <h3>Resultado da conversão:</h3>
        <p><strong>Grau em Fahrenheit:</strong> {$grau}</p>
        <p><strong>Grau em Celsius</strong> {$conversao}</p>
        ";
    }
}


?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Fahrenheit para Celsius</title>
    <link rel="stylesheet" href="style/style.css">
</head>

<body>

    <section class="container">
        <div class="content-90-780">

            <h1>Fahrenheit para<strong>Celsius</strong></h1>
            <form action="" method="GET">

                <a href="?">Atualizar</a>

                <?= $msgErro; ?>

                <label class="box100"><strong>Grau em Fahrenheit:</strong>
                    <input type="text" name="grau" placeholder="Informe o grau em Fahrenheit." />
                </label>

                <div class="box100">
                    <input type="submit" value="Enviar" />
                </div>
                <div class="clear"></div>


                <?= $msgSucesso; ?>
            </form>

            <div class="clear"></div>
        </div>
    </section>

</body>

</html>