<?php

/**
 * EXERCÍCIO:
 * Entrar com um número e informar se ele é divisível por 10, por 5, por 2 ou se não é divisível por nenhum destes.
 */
$msgErro = "";
$msgSucesso = "";

$numero = isset($_GET['numero']) ? $_GET['numero'] : 0;

if ($numero == "") {
    $msgErro = "<p class='erro'>Opss... Você precisa informar os <strong>Dois números</strong></p>";
} else {

    if ($numero > 0) {

        $numero = floatval($numero);

        $divisivelPorDois = $numero % 2;
        $divisivelPorCinco = $numero % 5;
        $divisivelPorDez = $numero % 10;

        $msgSucesso = "<h3>Resultado:</h3>";
        if ($divisivelPorDois == 0) {
            $msgSucesso .= "<p>O número <strong>{$numero}</strong> é divisível por <strong>2</strong></p>";
        } else {
            $msgSucesso .= "<p>O número <strong>{$numero}</strong> não é divisível por <strong>2</strong></p>";
        }

        if ($divisivelPorCinco == 0) {
            $msgSucesso .= "<p>O número <strong>{$numero}</strong> é divisível por <strong>5</strong></p>";
        } else {
            $msgSucesso .= "<p>O número <strong>{$numero}</strong> não é divisível por <strong>5</strong></p>";
        }

        if ($divisivelPorDez == 0) {
            $msgSucesso .= "<p>O número <strong>{$numero}</strong> é divisível por <strong>10</strong></p>";
        } else {
            $msgSucesso .= "<p>O número <strong>{$numero}</strong> não é divisível por <strong>10</strong></p>";
        }
    }
}

?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Número divisível por 10, 5 e 2</title>
    <link rel="stylesheet" href="style/style.css">
</head>

<body>

    <section class="container">
        <div class="content-90-780">

            <h1>Número divisível por <strong>10, 5 e 2</strong></h1>
            <form action="" method="GET">

                <a href="?">Atualizar</a>

                <?= $msgErro; ?>

                <label class="box100"><strong>Número:</strong>
                    <input type="text" name="numero" placeholder="Informe o número." />
                </label>

                <div class="box100">
                    <input type="submit" value="Enviar" />
                </div>
                <div class="clear"></div>


                <?= $msgSucesso; ?>
            </form>

            <div class="clear"></div>
        </div>
    </section>

</body>

</html>