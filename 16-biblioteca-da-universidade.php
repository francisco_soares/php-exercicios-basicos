<?php

/**
 * EXERCÍCIO:
 * A biblioteca de uma universidade deseja fazer um algoritmo que leia o nome do livro que será emprestado, o tipo de usuário (professor ou aluno) e possa imprimir um recibo conforme mostrado a seguir. Considerar que o professor tem 10 dias para devolver o livro o aluno somente 3 dias
 */
$msgErro = "";
$msgSucesso = "";

// Definindo a data e hora de São Paulo.
define("DATE_TIMEZONE", "America/Sao_Paulo");
date_default_timezone_set(DATE_TIMEZONE);

$livro = isset($_GET['livro']) ? $_GET['livro'] : 'nulo';
$usuario = isset($_GET['usuario']) ? $_GET['usuario'] : '';

if ($livro == '' || $usuario == '') {
    $msgErro = "<p class='erro'>Opss... Você precisa informar o <strong>Nome do livro</strong> e se você é <strong>Professor ou Aluno.</strong></p>";
} else {
    if ($usuario == 'professor') {
        $dataDevolucaoLivro = date('d/m/Y', strtotime('+10days'));

        $msgSucesso = "
        <h3>Resultado</h3>
        <p><strong>Nome do Livro:</strong> " . ucfirst($livro) . ".</p>        
        <p><strong>Usuário:</strong> " . ucfirst($usuario) . ".</p>        
        <p><strong>Data da devolução:</strong> {$dataDevolucaoLivro}.</p>
        ";
    } else {
        $dataDevolucaoLivro = date('d/m/Y', strtotime('+3days'));

        $msgSucesso = "
        <h3>Resultado</h3>
        <p><strong>Nome do Livro:</strong> " . ucfirst($livro) . ".</p>        
        <p><strong>Usuário:</strong> " . ucfirst($usuario) . ".</p>        
        <p><strong>Data da devolução:</strong> {$dataDevolucaoLivro}.</p>
        ";
    }
}



?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Biblioteca da Universidade</title>
    <link rel="stylesheet" href="style/style.css">
</head>

<body>

    <section class="container">
        <div class="content-90-780">

            <h1>Biblioteca da <strong>Universidade</strong></h1>
            <form action="" method="GET">

                <a href="?">Atualizar</a>

                <?= $msgErro; ?>

                <label class="box100"><strong>Livro:</strong>
                    <input type="text" name="livro" placeholder="Informe o nome do livro:" />
                </label>

                <label class="box100"><strong>Você é:</strong>
                    Professor: <input type="radio" name="usuario" value="professor" />
                    Aluno: <input type="radio" name="usuario" value="aluno" />
                </label>

                <div class="box100">
                    <input type="submit" value="Enviar" />
                </div>
                <div class="clear"></div>


                <?= $msgSucesso; ?>
            </form>

            <div class="clear"></div>
        </div>
    </section>

</body>

</html>