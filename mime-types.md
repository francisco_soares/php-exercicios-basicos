# Tipos de MIME TYPE

Adobe PDF	pdf	application/pdf	<br>
XML	xml	text/xml	<br>
Text	txt, asc	text/plain	<br>
HTML	htm, html	text/html	<br>
CSS	css	text/css	<br>
Microsoft Word	doc	application/msword	<br>
Microsoft Word XML	docx	application/vnd.openxmlformats-officedocument.wordprocessingml.document	<br>
Microsoft Powerpoint	ppt	application/vnd.ms-powerpoint	<br>
Microsoft Powerpoint XML	pptx	application/vnd.openxmlformats-officedocument.presentationml.presentation	<br>
Microsoft Excel	xls	application/vnd.ms-excel	<br>
Microsoft Excel XML	xlsx	application/vnd.openxmlformats-officedocument.spreadsheetml.sheet	<br>
MARC		application/marc	<br>
JPEG	jpeg, jpg	image/jpeg	<br>
GIF	gif	image/gif	<br>
image/png	png	image/png	<br>
TIFF	tiff, tif	image/tiff	<br>
AIFF	aiff, aif, aifc	audio/x-aiff	<br>
audio/basic	au, snd	audio/basic	<br>
WAV	wav	audio/x-wav	<br>
MPEG	mpeg, mpg, mpe	video/mpeg	<br>
RTF	rtf	text/richtext	<br>
Microsoft Visio	vsd	application/vnd.visio	<br>
FMP3	fm	application/x-filemaker	<br>
BMP	bmp	image/x-ms-bmp	<br>
Photoshop	psd, pdd	application/x-photoshop	<br>
Postscript	ps, eps, ai	application/postscript	<br>
Video Quicktime	mov, qt	video/quicktime	<br>
MPEG Audio	mpa, abs, mpega	audio/x-mpeg	<br>
Microsoft Project	mpp, mpx, mpd	application/vnd.ms-project	<br>
Mathematica	ma	application/mathematica	<br>
LateX	latex	application/x-latex	<br>
TeX	tex	application/x-tex	<br>
TeX dvi	dvi	application/x-dvi	<br>
SGML	sgm, sgml	application/sgml	<br>
WordPerfect	wpd	application/wordperfect5.1	<br>
RealAudio	ra, ram	audio/x-pn-realaudio	<br>
Photo CD	pcd	image/x-photo-cd	<br>
OpenDocument Text	odt	application/vnd.oasis.opendocument.text	<br>
OpenDocument Text Template	ott	application/vnd.oasis.opendocument.text-template	<br>
OpenDocument HTML Template	oth	application/vnd.oasis.opendocument.text-web	<br>
OpenDocument Master Document	odm	application/vnd.oasis.opendocument.text-master	<br>
OpenDocument Drawing	odg	application/vnd.oasis.opendocument.graphics	<br>
OpenDocument Drawing Template	otg	application/vnd.oasis.opendocument.graphics-template	<br>
OpenDocument Presentation	odp	application/vnd.oasis.opendocument.presentation	<br>
OpenDocument Presentation Template	otp	application/vnd.oasis.opendocument.presentation-template	<br>
OpenDocument Spreadsheet	ods	application/vnd.oasis.opendocument.spreadsheet	<br>
OpenDocument Spreadsheet Template	ots	application/vnd.oasis.opendocument.spreadsheet-template	<br>
OpenDocument Chart	odc	application/vnd.oasis.opendocument.chart	<br>
OpenDocument Formula	odf	application/vnd.oasis.opendocument.formula	<br>
OpenDocument Database	odb	application/vnd.oasis.opendocument.database	<br>
OpenDocument Image	odi	application/vnd.oasis.opendocument.image	<br>
OpenOffice.org extension	oxt	application/vnd.openofficeorg.extension	<br>
Writer 6.0 documents	sxw	application/vnd.sun.xml.writer	<br>
Writer 6.0 templates	stw	application/vnd.sun.xml.writer.template	<br>
Calc 6.0 spreadsheets	sxc	application/vnd.sun.xml.calc	<br>
Calc 6.0 templates	stc	application/vnd.sun.xml.calc.template	<br>
Draw 6.0 documents	sxd	application/vnd.sun.xml.draw	<br>
Draw 6.0 templates	std	application/vnd.sun.xml.draw.template	<br>
Impress 6.0 presentations	sxi	application/vnd.sun.xml.impress	<br>
Impress 6.0 templates	sti	application/vnd.sun.xml.impress.template	<br>
Writer 6.0 global documents	sxg	application/vnd.sun.xml.writer.global	<br>
Math 6.0 documents	sxm	application/vnd.sun.xml.math	<br>
StarWriter 5.x documents	sdw	application/vnd.stardivision.writer	<br>
StarWriter 5.x global documents	sgl	application/vnd.stardivision.writer-global	<br>
StarCalc 5.x spreadsheets	sdc	application/vnd.stardivision.calc	<br>
StarDraw 5.x documents	sda	application/vnd.stardivision.draw	<br>
StarImpress 5.x presentations	sdd	application/vnd.stardivision.impress	<br>
StarImpress Packed 5.x files	sdp	application/vnd.stardivision.impress-packed	<br>
StarMath 5.x documents	smf	application/vnd.stardivision.math	<br>
StarChart 5.x documents	sds	application/vnd.stardivision.chart	<br>
StarMail 5.x mail files	sdm	application/vnd.stardivision.mail	<br>
RDF XML	rdf	application/rdf+xml; charset=utf-8	<br>
WMV	wmv	audio/x-ms-wmv	<br>
MP4	mp4	video/mp4	<br>
AVI	avi	video/avi	<br>
SWF	swf	application/x-shockwave-flash	<br>
FLV	flv	video/x-flv	<br>
MP3 Audio	mp3	audio/mp3	<br>