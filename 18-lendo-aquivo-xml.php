<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lendo arquivo XML</title>
    <link rel="stylesheet" href="style/style.css">
</head>

<body>

    <section class="container">
        <div class="content-90-780">


            <h1>Lendo arquivo <strong>XML</strong></h1>

            <?php
            $xmlFile = simplexml_load_file(__DIR__ . '/file/18-lendo-arquivo-xml.xml');

            function real($valor){
                return number_format($valor, 2, ",", ".");
            }

            foreach ($xmlFile->livro as $livro) {
                echo "<br>";
                echo "<p><strong>Título</strong> {$livro->titulo}</p>";
                echo "<p><strong>Genero: </strong> {$livro->genero}</p>";
                echo "<p><strong>Autor: </strong> {$livro->autor}</p>";
                echo "<p><strong>Preço: </strong> R$ " . real(floatval($livro->valor)) ."</p>";
                echo "<br>";
            }

            ?>

            <div class="clear"></div>
        </div>
    </section>

</body>

</html>