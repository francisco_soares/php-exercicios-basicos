<?php

/**
 * EXERCÍCIO:
 * Faça um script para uma loja de tintas. O script deverá pedir o tamanho em metros quadrados da área a ser pintada. Considere que a cobertura da tinta é de 1 litro para cada 3 metros quadrados e que a tinta é vendida em latas de 18 litros, que custam R$ 80,00. Informe ao usuário a quantidades de latas de tinta a serem compradas e o preço total.
 */
$msgErro = "";
$msgSucesso = "";

function paraReal($valor){
    return number_format($valor, 2, ",", ".");
}

$metroQuadrado = isset($_GET['metroQuadrado']) ? $_GET['metroQuadrado'] : 0;

if ($metroQuadrado == "") {
    $msgErro = "<p class='erro'>Opss... Você precisa informar o <strong>METRO QUADRADO</strong>.</p>";
} else {

    $metroQuadrado = floatval($metroQuadrado);

    if ($metroQuadrado > 0) {
        $litros = $metroQuadrado / 3;
        $valorPorLitro = 80 / 18;
        $qtdLatas = ceil($litros / 18);
        $valorTotal = ($litros <= 18) ? 80 : 80 * $qtdLatas;

        $msgSucesso = "
        <h3>Total da sua compra:</h3>
        <p><strong>Quantidade de Litros:</strong> {$litros}</p>
        <p><strong>Quantidade de lata(s):</strong> {$qtdLatas}</p>
        <p><strong>Valor por litro:</strong> R$ " . paraReal($valorPorLitro) . "</p>
        <p><strong>Valor total</strong> R$ " . paraReal($valorTotal) . "</p>        
        ";
    }
}



?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Impressão de texto em escada</title>
    <link rel="stylesheet" href="style/style.css">
</head>

<body>

    <section class="container">
        <div class="content-90-780">

            <h1>Loja de <strong>Tintas</strong></h1>
            <form action="" method="GET">

                <a href="?">Atualizar</a>

                <?= $msgErro; ?>

                <label class="box100"><strong>Metro quadrado:</strong>
                    <input type="text" name="metroQuadrado" placeholder="Informe quantos a quantidade de metro quadrado:" />
                </label>

                <div class="box100">
                    <input type="submit" value="Enviar" />
                </div>
                <div class="clear"></div>


                <?= $msgSucesso; ?>
            </form>

            <div class="clear"></div>
        </div>
    </section>

</body>

</html>