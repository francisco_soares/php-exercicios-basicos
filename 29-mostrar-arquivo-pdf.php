<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mostrar arquivos PDF</title>

    <!-- Bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">

    <style>
        #show_data {
            display: none;
        }
    </style>
</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col pt-5">
                <form class="row g-3" id="form_pdf" method="POST">
                    <div class="col-md-5">
                        <label for="ano">Escolha o Ano:</label>
                        <select class="form-select" name="ano" id="ano" aria-label="Default select example">
                            <option value="2020" selected>2020</option>
                            <option value="2021">2021</option>
                            <option value="2022">2022</option>
                            <option value="2023">2023</option>
                        </select>
                    </div>
                    <div class="col-md-5">
                        <label for="mes">Escolha o Mês:</label>
                        <select class="form-select" name="mes" id="mes" aria-label="Default select example">
                            <option value="01" selected>Janeiro</option>
                            <option value="02">Fevereiro</option>
                            <option value="03">Março</option>
                            <option value="04">Abril</option>
                            <option value="05">Maio</option>
                            <option value="06">Junho</option>
                            <option value="07">Julho</option>
                            <option value="08">Agosto</option>
                            <option value="09">Setembro</option>
                            <option value="10">Outubro</option>
                            <option value="11">Novembro</option>
                            <option value="12">Dezembro</option>
                        </select>
                    </div>
                    <div class="col-auto d-flex align-content-end flex-wrap">
                        <button type="submit" class="btn btn-primary btn_send">Buscar PDF</button>
                    </div>
                </form>
            </div>
        </div>

        <div class="row mt-5" id="show_data">
            <div class="col">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">Arquivo</th>
                            <th scope="col" class="text-end">Download Liberado</th>
                        </tr>
                    </thead>
                    <tbody id="tbody_pdf">
                    </tbody>
                </table>
            </div>
        </div>
    </div>


    <!-- Bootstrap -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>

    <script src="https://code.jquery.com/jquery-3.6.4.js" integrity="sha256-a9jBBRygX1Bh5lt8GZjXDzyOB+bWve9EiO7tROUtj/E=" crossorigin="anonymous"></script>

    <script>
        jQuery(function() {

            $("#ano").on('change', function() {
                $("#show_data").css({
                    "display": "none"
                });
            });

            $("#mes").on('change', function() {
                $("#show_data").css({
                    "display": "none"
                });
            });

            jQuery("#form_pdf").on("submit", function(e) {
                e.preventDefault();
                let data = jQuery(this).serialize();

                jQuery.ajax({
                    url: "request/getFilePdf.php",
                    data: data,
                    type: "POST",
                    dataType: "json",
                    success: (data_return) => {

                        let tbody_pdf = jQuery("#tbody_pdf");
                        tbody_pdf.html('');

                        if (data_return.success) {
                            let dir_replace = data_return.dir.replace('../', '');

                            $("#show_data").css({
                                "display": "initial"
                            });

                            jQuery.each(data_return.files, (i, val) => {
                                let path = dir_replace + val;

                                tbody_pdf.append("<tr><td><a target='_blank' href='" + path + "'>" + val + "</a></td><td class='text-end'><a target='_blank' class='btn btn-success' href='" + path + "' download='" + val + "'>Fazer Download</a></td></tr>");
                            });
                        }

                        if (data_return.error) {
                            $("#show_data").css({
                                "display": "initial"
                            });
                            tbody_pdf.append("<tr><td><p>Opss... Não encontramos nenhum arquivo</p></td><td></td></tr>");
                        }

                    }
                });
            });
        });
    </script>
</body>

</html>